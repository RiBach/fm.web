import React from "react";

const Card = ({ children }) => {
  const cardStyle = {
    padding: "10px",
    margin: "20px",
    marginBottom: "0px",
    boxShadow: "5px 3px 10px 0 rgba(0,0,0,0.4)",
    border: "3px solid #64aef2",
    borderRadius: "30px",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  };

  return <div style={cardStyle}>{children}</div>;
};

export default Card;
