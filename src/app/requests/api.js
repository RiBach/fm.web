import axios from "axios";
import { BASE_URL } from "./config";

class Api {
  // Environment
  static getLatestTemperature() {
    return axios
      .get(`${BASE_URL}/latest/temperature`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getRangedTemperature(startDate, endDate, identifier) {
    const latestTime = new Date();
    const earliestTime = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);

    return axios
      .get(`${BASE_URL}/range/temperature`, {
        params: {
          identifier: identifier,
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },

      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getLatestHumidity() {
    return axios
      .get(`${BASE_URL}/latest/humidity`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getRangedHumidity(startDate, endDate, identifier) {
    const latestTime = new Date();
    const earliestTime = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);

    return axios
      .get(`${BASE_URL}/range/humidity`, {
        params: {
          identifier: identifier,
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getBatteryLevels() {
    return axios
      .get(`${BASE_URL}/latest/battery`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  //_______________________________________________________________________________________
  // Energy
  static getLatestPower() {
    return axios
      .get(`${BASE_URL}/latest/power`)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getRangedPower(startDate, endDate) {
    const latestTime = new Date();
    const earliestTime = new Date(new Date().getTime() - 1 * 60 * 60 * 1000);

    return axios
      .get(`${BASE_URL}/range/power`, {
        params: {
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getRangedEnergyConsumption(startDate, endDate) {
    const latestTime = new Date();
    const earliestTime = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);

    return axios
      .get(`${BASE_URL}/range/energy`, {
        params: {
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  //_______________________________________________________________________________________
  // Compressor
  static getRangedAcceleration(startDate, endDate) {
    return axios
      .get(`${BASE_URL}/acceleration`, {
        params: {
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }

  static getRangedMagneticField(startDate, endDate) {
    return axios
      .get(`${BASE_URL}/magc`, {
        params: {
          startTime: earliestTime.toISOString(),
          endTime: latestTime.toISOString(),
        },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        console.error("Error fetching data: ", error);
      });
  }
}

export default Api;
