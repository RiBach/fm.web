"use client";

import Card from "./components/card";
import React, { useEffect, useState } from "react";
import Api from "./requests/api";
import { Line } from "react-chartjs-2";
import { CrosshairPlugin, Interpolate } from "chartjs-plugin-crosshair";
import "chartjs-adapter-date-fns";
import moment from "moment-timezone";
import { useSpring, animated } from "react-spring";

import {
  Chart as ChartJS,
  LineElement,
  CategoryScale, // x axis
  LinearScale, // y axis
  PointElement,
  Legend,
  Tooltip,
  Filler,
  Interaction,
  TimeScale,
} from "chart.js";

ChartJS.register(
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  Legend,
  Tooltip,
  Filler,
  CrosshairPlugin,
  TimeScale
);
Interaction.modes.interpolate = Interpolate;

function AnimatedNumber({ previousNumber, newNumber, decimals = 0 }) {
  const props = useSpring({
    from: { number: previousNumber },
    to: { number: newNumber },
    config: { mass: 1, tension: 80, friction: 20 },
  });

  return (
    <animated.span>{props.number.to((n) => n.toFixed(decimals))}</animated.span>
  );
}

function downsampleData(data) {
  let factor = 1;
  data.length > 1000 ? (factor = data.length / 1000) : (factor = 1);
  factor = Math.floor(factor);
  return data.filter((_, index) => index % factor === 0);
}

function generate(x1, x2) {
  if (moment.isMoment(x1)) {
    // translate time to x1, x2
    x1 =
      -2 -
      moment()
        .add(-2 * 86400, "seconds")
        .diff(x1, "seconds") /
        86400;
    x2 =
      2 -
      moment()
        .add(2 * 86400, "seconds")
        .diff(x2, "seconds") /
        86400;
  }

  const t = moment().add(x1 * 86400, "seconds");
  const t2 = moment().add(x2 * 86400, "seconds");

  return { x1, x2 };
}

export default function Home() {
  const [latestTemperatureData, setLatestTemperatureData] = useState([]);
  const [previousLatestTemperatureData, setPreviousLatestTemperatureData] = useState([]);
  const [temperatureRangeData, setTemperatureRangeData] = useState([]);

  const [latestHumidityData, setLatestHumidityData] = useState([]);
  const [previousLatestHumidityData, setPreviousLatestHumidityData] = useState([]);
  const [humidityRangeData, setHumidityRangeData] = useState([]);

  const [latestPowerData, setLatestPowerData] = useState([]);
  const [previousLatestPowerData, setPreviousLatestPowerData] = useState([]);
  const [powerRangeData, setPowerRangeData] = useState([]);

  const [energyAverage, setEnergyAverage] = useState([]);
  const [previousEnergyAverage, setPreviousEnergyAverage] = useState([]);

  const [zoomedData, setZoomedData] = useState([]);
  const [dataset, setDataset] = useState([]);

  const [showContainer, setShowContainer] = useState(false);


  useEffect(() => {
    Api.getLatestTemperature().then((data) => {
      setLatestTemperatureData(data);
    });
    const intervalId = setInterval(() => {
      Api.getLatestTemperature().then((data) => {
        setLatestTemperatureData((prev) => {
          setPreviousLatestTemperatureData(prev);
          return data;
        });
      });
    }, 10000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    Api.getRangedTemperature(null, null, "lower_temperature").then((data) => {
      setTemperatureRangeData(data);
    });
    const intervalId = setInterval(() => {
      Api.getRangedTemperature(null, null, "lower_temperature").then((data) => {
        setTemperatureRangeData(data);
      });
    }, 1000000);
    return () => clearInterval(intervalId);
  }, []);

    useEffect(() => {
    Api.getRangedHumidity(null, null, "lower_humidity").then((data) => {
      setHumidityRangeData(data);
    });
    const intervalId = setInterval(() => {
      Api.getRangedHumidity(null, null, "lower_humidity").then((data) => {
        setHumidityRangeData(data);
      });
    }, 1000000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    Api.getLatestHumidity().then((data) => {
      setLatestHumidityData(data);
    });
    const intervalId = setInterval(() => {
      Api.getLatestHumidity().then((data) => {
        setLatestHumidityData((prev) => {
          setPreviousLatestHumidityData(prev);
          return data;
        });
      });
    }, 10000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    Api.getLatestPower().then((data) => {
      setLatestPowerData(data);
    });
    const intervalId = setInterval(() => {
      Api.getLatestPower().then((data) => {
        setLatestPowerData((prev) => {
          setPreviousLatestPowerData(prev);
          return data;
        });
      });
    }, 10000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    Api.getRangedPower().then((data) => {
      setPowerRangeData(data);
    });
    const intervalId = setInterval(() => {
      Api.getRangedPower().then((data) => {
        setPowerRangeData(data);
      });
    }, 1000000);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    Api.getRangedEnergyConsumption().then((data) => {
      setEnergyAverage(data);
    });
    const intervalId = setInterval(() => {
      Api.getRangedEnergyConsumption().then((data) => {
        setEnergyAverage((prev) => {
          setPreviousEnergyAverage(prev);
          return data;
        });
      });
    }, 10000);
    return () => clearInterval(intervalId);
  }, []);

  const handleContainerClick = () => {
    if (!showContainer) {
      setShowContainer(true);
    }
  };

  // too much data laggs the browser
  // some function to limit the data displayed

  const sampledData = downsampleData(dataset);
  const data = sampledData && {
    labels: sampledData
      .slice()
      .reverse()
      .map((data) => moment.utc(data.timestamp).tz(moment.tz.guess()).format()),
    datasets: [
      {
        label: "Wattage",
        data: sampledData
          .slice()
          .reverse()
          .map((data) => data.value),
        borderColor: "#2daef2",
        borderWidth: 2,
        pointBorderColor: "#2daef2",
        pointBorderWidth: 0,
        pointRadius: 0,
        tension: 0.3,
        fill: false,
        backgroundColor: (context) => {
          const ctx = context.chart.ctx;
          const gradient = ctx.createLinearGradient(0, 0, 0, 300);
          gradient.addColorStop(0, "#2daef2");
          gradient.addColorStop(1, "white");
          return gradient;
        },
      },
    ],
  };

  const options = {
    plugins: {
      legend: false,
      tooltip: {
        mode: "nearest",
        intersect: false,
      },
      crosshair: {
        line: {
          color: "#2daef2", // crosshair line color
          width: 1, // crosshair line width
        },
        sync: {
          enabled: true, // enable trace line syncing with other charts
          group: 1, // chart group
          suppressTooltips: false, // suppress tooltips when showing a synced tracer
        },
        zoom: {
          enabled: false, // enable zooming
          zoomboxBackgroundColor: "rgba(90, 92, 105, 0.4)", // background color of zoom box
          zoomboxBorderColor: "rgba(90, 92, 105, 0.2)", // border color of zoom box
          zoomButtonText: "Reset Zoom", // reset zoom button text
          zoomButtonClass: "reset-zoom", // reset zoom button class
        },
        callbacks: {
          afterZoom: function (start, end) {
            console.log(start);
            console.log(generate(moment(start), moment(end)));

            // setZoomedData(powerRangeData.slice(startIndex, endIndex + 1));
          },
        },
      },
    },

    adapters: {
      date: {
        timeZone: "local",
      },
    },

    responsive: true,
    scales: {
      y: {
        grid: {
          color: "grey", // change to desired color for x-axis gridlines
        },
        ticks: {
          color: "white",
          font: {
            size: 14,
          },
        },
        title: {
          display: false,
          text: "Watts",
          padding: {
            bottom: 10,
          },
          font: {
            size: 20,
            family: "Arial",
          },
        },
      },
      x: {
        type: "time",
        time: {
          unit: "minute",
          displayFormats: {
            minute: "d/MM HH:mm",
          },
        },
        grid: {
          color: "grey", // change to desired color for x-axis gridlines
        },
        ticks: {
          color: "white",
          font: {
            size: 14,
          },
          maxTicksLimit: 13,
        },
        max: sampledData.length > 0
        ? moment.utc(sampledData[0].timestamp).tz(moment.tz.guess()).format()
        : null
      },
    },
  };

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-2">
      <div>
        <button onClick={() => {}}>Debug</button>
      </div>



      {showContainer ? (
        <div>
          <Card>
            {latestPowerData && (
              <div>
                <div>
                  <h1 className="font-bold text-2xl text-right mt-2 mr-2">
                    <span
                      onClick={() => {
                        setShowContainer(false);
                      }}
                      style={{ cursor: "pointer", padding: "7px" }}
                    >
                      X
                    </span>
                  </h1>
                </div>

                <div className="mb-1 grid text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-3 lg:text-center">
                  <span> </span>
                  <h1 className="font-bold text-2xl text-center mt-10">
                    Power
                  </h1>
                </div>
                <div
                  style={{
                    width: "90vw",
                    height: "60vh",
                    padding: "20px",
                    cursor: "pointer",
                    justifyContent: "center",
                    alignItems: "center",
                    display: "flex",
                  }}
                >
                  <Line
                    style={{ width: "100%" }}
                    data={data}
                    options={options}
                  ></Line>
                </div>
              </div>
            )}
          </Card>
        </div>

      ) : (

        <Card>

          {Object.keys(previousEnergyAverage).length > 0 ?  (
            <Card>
              <h1 className="font-bold text-center mt-1"> Energy Consumption last 24 hours </h1>
              <div>
                <h2 className="text-center m-5">
                    <AnimatedNumber
                    previousNumber={previousEnergyAverage.kiloWattHours}
                    newNumber={energyAverage.kiloWattHours}
                    decimals={3}
                    ></AnimatedNumber>{" "}kWh
                </h2>
                <h2 className="text-center m-5">
                    <AnimatedNumber
                    previousNumber={previousEnergyAverage.wattMinutes}
                    newNumber={energyAverage.wattMinutes}
                    ></AnimatedNumber>{" "} Wm
                </h2>
              </div>
            </Card>
            ) : (
              <Card>
                <h1 className="font-bold text-center mt-1">Energy Consumption last 24 hours</h1>
                <div>
                    <h2 className="text-center m-5">{energyAverage.kiloWattHours} kWh</h2>
                    <h2 className="text-center m-5">{energyAverage.wattMinutes} Wm</h2>
                </div>
              </Card>
            )}


          {Object.keys(previousLatestPowerData).length > 0 ? (
            <Card>
              <h1 className="font-bold text-center mt-1">Power</h1>
              <div>
                <h2 className="text-center m-5">
                  <AnimatedNumber
                    previousNumber={previousLatestPowerData.value}
                    newNumber={latestPowerData.value}
                    decimals={2}
                  ></AnimatedNumber>{" "}
                  W
                </h2>
              </div>
            </Card>
          ) : (
            <Card>
              <h1 className="font-bold text-center mt-1">Power</h1>
              <div>
                <h2 className="text-center m-5">{latestPowerData.value} W</h2>
              </div>
            </Card>
            )}


            <Card>
              <h1 className="font-bold text-center mt-1">Temperature</h1>
              {Object.keys(previousLatestTemperatureData).length > 0 ? (
                <div>
                  <h2 className="text-1xl text-center m-5">
                    {latestTemperatureData.map((data, index) => (
                      <div key={index}>
                        {data.identifier}:{" "}
                        <AnimatedNumber
                        previousNumber={previousLatestTemperatureData[index].value / 10.0}
                        newNumber={data.value / 10.0}
                        decimals={1}
                        />{" "}°C
                      </div>
                    ))}
                  </h2>
                </div>
              ) : (
                <div>
                  <h2 className="text-1xl text-center m-5">
                    {latestTemperatureData.map((data, index) => (
                      <div key={index}>
                        {data.identifier}: {data.value / 10.0} °C
                      </div>
                    ))}
                  </h2>
                </div>
              )}
            </Card>

            <Card>
              <h1 className="font-bold text-center mt-1">Humidity</h1>
              {Object.keys(previousLatestHumidityData).length > 0 ? (
                <div>
                    <h2 className="text-1xl text-center m-5">
                      {latestHumidityData.map((data, index) => (
                        <div key={index}>
                          {data.identifier}:{" "}
                          <AnimatedNumber
                          previousNumber={previousLatestHumidityData[index].value / 10.0}
                          newNumber={data.value / 10.0}
                          decimals={1}
                          />{" "}%
                        </div>
                      ))}
                    </h2>
                  </div>
                ) : (
                  <div>
                    <h2 className="text-1xl text-center m-5">
                      {latestHumidityData.map((data, index) => (
                        <div key={index}>
                          {data.identifier}: {data.value / 10.0} %
                        </div>
                      ))}
                    </h2>
                  </div>
                )}
            </Card>


          </Card>
      )}





      <div className="grid text-center w-full mb-1 grid-cols-5 mt-3 mb-2">
        <a
          onClick={() => {
            setDataset(powerRangeData);
            handleContainerClick();
          }}
          className="group rounded-lg text-center cursor-pointer border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`m-2 text-2xl font-semibold`}>
            PWR{" "}
          </h2>
        </a>


        <a
          onClick={() => {
            setDataset(temperatureRangeData);
            handleContainerClick();
          }}
          className="group rounded-lg text-center cursor-pointer border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`m-2 text-2xl font-semibold`}>
            T{" "}
          </h2>
        </a>


        <a
          onClick={() => {
            setDataset(humidityRangeData);
            handleContainerClick();
          }}
          className="group rounded-lg text-center cursor-pointer border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`m-2 text-2xl font-semibold`}>
            H%{" "}
          </h2>
        </a>


        <a
          onClick={() => {
            setDataset(temperatureRangeData);
            handleContainerClick();
          }}
          className="group rounded-lg text-center cursor-pointer border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`m-2 text-2xl font-semibold`}>
            NRG{" "}
          </h2>
        </a>


        <a
          onClick={() => {
            setDataset(temperatureRangeData);
            handleContainerClick();
          }}
          className="group rounded-lg text-center cursor-pointer border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`m-2 text-2xl font-semibold`}>
            CMPR(null){" "}
          </h2>
        </a>


      </div>
    </main>
  );
}
